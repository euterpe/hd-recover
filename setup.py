from setuptools import setup, find_packages


def readme():
    with open('README.md', 'r') as readme:
        return readme.read()


setup(
    name='hd_recover',
    version='0.3.8',
    author='Jon Badiali',
    author_email='jon@jonbadiali.com',
    description='File recovery sorting utility.',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities'
    ],
    keywords='file recovery filesystem sort',
    url='https://gitlab.com/euterpe/hd-recover',
    license='BSD',
    packages=find_packages(),
    install_requires=[
        'hsaudiotag3k',
        'pyyaml',
        'docopt',
        'Pillow',
    ],
    entry_points={
        'console_scripts': [
            'hd_recover=scripts.cli:main']
    },
    include_package_data=True,
)
