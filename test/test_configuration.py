import unittest
from unittest import mock
from hd_recover import configuration

# --- BEGIN TEST CONFIGURATION METHODS --- #


class ConfigurationTestCase(unittest.TestCase):

    def test_validate_config(self):
        config = configuration.DEFAULT_CONFIG

        # Check program exits for following config settings:
        #   min_size not int
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['min_size'] = 'test'
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   min_size < 0
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['min_size'] = -1
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   dir_size not int
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['dir_size'] = 'test'
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   dir_size < 0
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['dir_size'] = -1
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   img_mins not dict
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['img_mins'] = None
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   img_mins['pixels'] not int
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['img_mins'] = {'pixels': None, 'size': None}
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   img_mins['pixels'] < 0
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['img_mins']['pixels'] = -1
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   img_mins['size'] not int
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['img_mins'] = {'pixels': None, 'size': None}
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   img_mins['size'] < 0
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['img_mins']['size'] = -1
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   ignore_blanks not bool
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['ignore_blanks'] = None
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   ignore_thumbnails not bool
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['ignore_thumbnails'] = None
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   img_formats not list
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['img_formats'] = None
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   format in img_formats not str
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['img_formats'] = ['mp3', 10, False]
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   extensions not dict
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['extensions'] = None
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

        #   extensions[extension] not bool
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            config['extensions'] = {'mp3': True, 'jpg': 25}
            fake_sys.exit.side_effect = SystemExit
            try:
                configuration._validate_config(config)
            except SystemExit:
                assert True
            else:
                assert False
            fake_sys.exit.assert_called_once_with()

    def test_populate_missing_config(self):
        # Check incomplete configuration correctly populated from default
        config = {}
        config = configuration._populate_missing_config(config)
        self.assertEqual(config, configuration.DEFAULT_CONFIG)

        # Check program exits on KeyError from incomplete default config
        with mock.patch('hd_recover.configuration.sys') as fake_sys:
            with mock.patch('hd_recover.configuration.DEFAULT_CONFIG', {}):
                config = {}
                configuration._populate_missing_config(config)
                fake_sys.exit.assert_called_once_with()

    def test_load_config(self):
        # Check 'open' called with valid input file
        with mock.patch('hd_recover.configuration._validate_config',
                        return_value=True) as fake_validate:
            with mock.patch('hd_recover.configuration.open') as fake_open:
                with mock.patch('hd_recover.configuration.yaml') as fake_yaml:
                    fake_yaml.load.return_value = {
                        'extensions': {'mp3': True, 'gif': True},
                        'img_formats': ['jpg', 'gif']
                    }
                    configuration.load_config(filepath='some file')
                    fake_yaml.load.assert_called_once_with(
                        fake_open.return_value.__enter__.return_value)

        # Check default program exit if filepath not found
        with mock.patch('hd_recover.configuration._validate_config',
                        return_value=True) as fake_validate:
            with mock.patch('hd_recover.configuration.open') as fake_open:
                with mock.patch('hd_recover.configuration.sys') as fake_sys:
                    fake_open.side_effect = FileNotFoundError
                    fake_sys.exit.side_effect = SystemExit
                    try:
                        config = configuration.load_config(
                            filepath='some file')
                    except SystemExit:
                        assert True
                    else:
                        assert False
                    fake_sys.exit.assert_called_once_with()

        # Check default config file loaded if no filepath specified
        with mock.patch('hd_recover.configuration._validate_config',
                        return_value=True) as fake_validate:
            with mock.patch('hd_recover.configuration.open') as fake_open:
                with mock.patch('hd_recover.configuration.yaml') as fake_yaml:
                    fake_yaml.load.return_value = configuration.DEFAULT_CONFIG
                    config = configuration.load_config()
                    fake_yaml.load.assert_called_once_with(
                        fake_open.return_value.__enter__.return_value)
                    self.assertEqual(configuration.DEFAULT_CONFIG, config)

        # Check default configuration loaded if default config file not found
        with mock.patch('hd_recover.configuration._validate_config',
                        return_value=True) as fake_validate:
            with mock.patch('hd_recover.configuration.open') as fake_open:
                fake_open.side_effect = FileNotFoundError
                config = configuration.load_config()
                self.assertEqual(configuration.DEFAULT_CONFIG, config)

        # Check object config overridden by CLI options:
        with mock.patch('hd_recover.configuration._validate_config',
                        return_value=True) as fake_validate:
            with mock.patch('hd_recover.configuration.open') as fake_open:
                with mock.patch('hd_recover.configuration.yaml') as fake_yaml:
                    fake_yaml.load.return_value = {
                        'img_mins': {}}
                    extensions = ['mp3', 'jpg', 'gif']
                    config = configuration.load_config(
                        indent=2, ignore_blanks=True, ignore_thumbnails=True,
                        file_size=5000, pixels=200, img_size=40000,
                        extensions=extensions
                    )

                    #   indent
                    self.assertEqual(2, config['indent'])
                    #   ignore_blanks
                    self.assertTrue(config['ignore_blanks'])
                    #   ignore_thumbnails
                    self.assertTrue(config['ignore_thumbnails'])
                    #   file_size
                    self.assertEqual(config['min_size'], 5000)
                    #   pixels
                    self.assertEqual(
                        config['img_mins']['pixels'], 200)
                    #   img_size
                    self.assertEqual(
                        config['img_mins']['size'], 40000)

                    # Check validate config called
                    fake_validate.assert_called_once_with(config)

        # Check all config fields have values when incomplete config supplied
        with mock.patch('hd_recover.configuration._validate_config',
                        return_value=True) as fake_validate:
            with mock.patch('hd_recover.configuration.open') as fake_open:
                with mock.patch('hd_recover.configuration.yaml') as fake_yaml:
                    fake_yaml.load.return_value = {'ignore_thumbnails': True}
                    config = configuration.load_config()

                    # Check validate config called
                    fake_validate.assert_called_once_with(config)

                    #   ignore_blanks
                    self.assertIsInstance(config['ignore_blanks'],
                                          bool)
                    #   ignore_thumbnails
                    self.assertIsInstance(config['ignore_thumbnails'],
                                          bool)
                    #   file_size
                    self.assertIsInstance(config['min_size'], int)
                    #   pixels
                    self.assertIsInstance(
                        config['img_mins']['pixels'], int)
                    #   img_size
                    self.assertIsInstance(
                        config['img_mins']['size'], int)

                    # extensions
                    self.assertIsInstance(config['extensions'], dict)

                    # Check if RecoverTool.extensions and
                    # RecoverTool.img_formats are populated
                    self.assertIsInstance(config['img_formats'], list)
                    self.assertIsInstance(config['extensions'], dict)

    # --- END TEST CONFIGURATION METHODS --- #


if __name__ == '__main__':
    unittest.main()
