import os
import json
from hdr_utils import dateof, sizeof, strip_extension, is_ignored
from configuration import load_config, DEFAULT_EXPORT


# ----- BEGIN ANALYSIS METHODS ----- #

def _initialize_extension_map(config):
    """
    Set initial results values for all extensions to enable incrementation
    and addition during metadata processing.

    :param config dict: map of configuration settings
    :returns extensions dict: map extension results all set to zero
    """
    extensions = {}
    for extension in config['extensions']:
        if config['extensions'][extension] is True:
            extensions[extension] = {'count': 0, 'size': 0, 'percent': 0}
    return extensions


def _list_directories():
    """
    Build list of all directories to analyze. Assumes all directories are
    children of current working directory.

    :returns directories list: list of all source directories
    """
    directories = []
    dir_contents = os.listdir()
    for item in dir_contents:
        if os.path.isdir(item):
            directories.append(item)
    return directories


def _get_file_metadata(directories, analysis):
    """
    Retrieve metadata from source files based on configuration settings.
    Data retrieved includes file extension, size, and date.

    :param directories list: directories to analyze
    :param analysis dict: map of unpopulated analysis results
    :param config dict: map of configuration settings
    :returns analysis dict: map of populated analysis results
    """

    for directory in directories:
        filenames = os.listdir(directory)    # List files
        for filename in filenames:  # Get each file's extension info
            filepath = directory + '/' + filename
            if is_ignored(filepath, analysis['config']):
                size = sizeof(filepath)
                analysis['ignored']['count'] += 1
                analysis['ignored']['size'] += size
            else:
                size = sizeof(filepath)
                extension = strip_extension(filename)
                analysis['extensions'][extension]['count'] += 1
                analysis['extensions'][extension]['size'] += size

                date = dateof(filepath)
                if date in analysis['dates']:
                    analysis['dates'][date]['count'] += 1
                else:
                    analysis['dates'][date] = {
                        'count': 1, 'size': 0, 'percent': 0}
                analysis['dates'][date]['size'] += size
            analysis['total_size'] += sizeof(filepath)

    return analysis


def _calculate_percentages(analysis):
    """
    Calculate percentage of total size for each file extension and file
    modification date from analysis results.

    :param analysis dict: map of analysis results
    :returns analysis dict: map of analysis results with updated percentages
    """
    if analysis['total_size'] > 0:
        for extension in analysis['extensions']:
            analysis['extensions'][extension]['percent'] = (
                analysis['extensions'][extension]['size']
                / analysis['total_size']) * 100
        for date in analysis['dates']:
            analysis['dates'][date]['percent'] = (
                analysis['dates'][date]['size'] / analysis['total_size']) * 100
    else:
        for extension in analysis['extensions']:
            analysis['extensions'][extension]['percent'] = 0
        for date in analysis['dates']:
            analysis['dates'][date]['percent'] = 0

    return analysis


def _export_to_json(analysis, filepath, indent=None):
    """
    Export analysis results as JSON object file.

    :param filename str: name of output file
    :param indent int: number of spaces to indent JSON data for readability
    """
    if not filepath:
        print('hd_recover: WARNING: no output file specified: '
              + 'analysis not exported.')
        return False

    print('hd_recover: exporting data to {0}... '.format(filepath), end='')
    try:
        with open(filepath, 'w') as json_file:
            json.dump(analysis, json_file, indent=indent)
    except NotADirectoryError:
        print('hd_recover: WARNING: invalid export file path: '
              + 'analysis not exported.')
        return False
    print('OK')


# ----- BEGIN PUBLIC METHOD ----- #

def hdr_analyze(ignore_blanks=None, ignore_thumbnails=None, file_size=None,
                pixels=None, img_size=None, extensions=None, output_file=None,
                indent=None, no_export=False):
    """
    Construct analysis of source files based on configuration settings
    and command line options then export as JSON file.

    :param ignore_blanks bool: whether to ignore mp3s with blank id3 tags
    :param ignore_thumbnails bool: whether to ignore images as specified by
            pixels, img_size, and img_formats from config file
    :param file_size int: minimum file size to include in analysis in bytes
    :param pixels int: minimum pixel dimensions for image files
    :param img_size int: minimum image file size in bytes
    :param extensions list: list of extensions to include in analysis
    :param output_file str: destination path for export analysis results
    :param indent int: number of spaces to indent JSON export for readability
    :param no_export bool: whether to export analysis results
    """

    print('hd_recover: loading configuration... ', end='')
    config = load_config(
        ignore_blanks=ignore_blanks,
        ignore_thumbnails=ignore_thumbnails,
        file_size=file_size,
        pixels=pixels,
        img_size=img_size,
        extensions=extensions
    )
    print('OK')

    print('hd_recover: initializing analysis... ', end='')
    analysis = {
        'extensions': {},
        'dates': {},
        'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
        'total_size': 0,
        'config': config
    }
    analysis['extensions'] = _initialize_extension_map(config)
    print('OK')

    print('hd_recover: identifying recovery directories... ', end='')
    directories = _list_directories()
    print('OK')

    print('hd_recover: retrieving file metadata... ', end='')
    analysis = _get_file_metadata(directories, analysis)
    print('OK')

    print('hd_recover: calculating extension distribution... ', end='')
    analysis = _calculate_percentages(analysis)
    print('OK')

    if not no_export:       # Export
        if output_file:
            _export_to_json(analysis, output_file, indent=2)
        else:
            _export_to_json(analysis, DEFAULT_EXPORT, indent=2)

    print('hd_recover: ANALYSIS COMPLETE: ready for assessment.')

# ----- END PUBLIC METHOD ----- #
