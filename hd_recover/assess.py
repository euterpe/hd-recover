import os
from hdr_utils import format_size, import_from_json


DEFAULT_IMPORT = 'hdr_analysis.json'


# ----- BEGIN PRIVATE METHOD ----- #

def _print_item(data, item, count):
    """
    Prints the analysis results for a single extension or date.

    :param data dict: map of analysis results for extensions or dates
    :param item str: the date or extension to print data for
    :param count int: index of current item to print
    :returns count int: index to use for next item
    """
    if data[item]['count'] > 0:
        print('{:5}'.format(str(count) + '. ')
              + '{:12}'.format(str(item) + '')
              + '{:>10}'.format(str(data[item]['count']))
              + '{:>10}'.format(format_size(data[item]['size']))
              + '{:>10.2f}'.format(data[item]['percent']) + '%'
              )
        count += 1
    return count

# ----- END PRIVATE METHOD ----- #


# ----- BEGIN PUBLIC METHOD ----- #

def hdr_assess(input_file=None, n=None, extensions=None, by_date=False,
               field=None, asc=False):
    """
    Print assessment details in human-readable format.
    Accepts several options detailed in 'Usage' docstring.

    Args:
        n (int): number of results to display
        ext (list): extensions to display results for
        by_date (bool): sort by date instead of extension
        field (str): metadata field to sort by
        asc (bool): ascending or descending order
    Returns:
        none
    """

    if input_file is None:
        if os.path.isfile(DEFAULT_IMPORT):
            analysis = import_from_json(DEFAULT_IMPORT)
    else:
        analysis = import_from_json(input_file)

    if field not in ('count', 'size', 'percent'):
        field = 'count'

    if n is not None:
        try:
            n = int(n)
        except ValueError:
            n = None

    if isinstance(n, int) and n <= 0:
        n = None

    if n and extensions:
        print(
            'hd_recover assess: ABORTING: cannot pass number and extension'
        )
        return

    if by_date is True:     # Sort by date
        sorted_items = sorted(
            analysis['dates'].keys(),
            key=lambda x: analysis['dates'][x][field],
            reverse=not asc)    # reverse==True is descending
    else:                   # Sort by extension
        if extensions:
            sorted_items = sorted(extensions, reverse=not asc)
        else:
            sorted_items = sorted(
                analysis['extensions'].keys(),
                key=lambda x: analysis['extensions'][x][field],
                reverse=not asc)

    print('\nIgnored files : '
          + '{:>10}'.format(str(analysis['ignored']['count']))
          + '{:>10}'.format(format_size(analysis['ignored']['size']))
          + '{:>12.2f}'.format(analysis['ignored']['percent'])
          + '%\n'
          )

    count = 1
    if n is None:
        n = len(sorted_items)
    else:
        n if n <= len(sorted_items) else len(sorted_items)
    for item in sorted_items[:n]:
        if by_date:
            count = _print_item(analysis['dates'], item, count)
        else:
            count = _print_item(analysis['extensions'], item, count)

    print('')

# ----- END PUBLIC METHOD ----- #
