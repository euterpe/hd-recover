import os
import sys
import yaml
import inspect


DEFAULT_EXPORT = 'hdr_analysis.json'
DEFAULT_IMPORT = 'hdr_analysis.json'


# --- BEGIN PRIVATE METHODS --- #

def _validate_config(config):
    """
    Check configuration file for invalid values by checking types
    and integer values.

    :param config dict: unvalidated map of configuration values
    :returns dict: valid map of configuration values
    """

    if type(config['min_size']) is not int:
        try:
            config['min_size'] = int(config['min_size'])
        except ValueError:
            print('hd_recover: ABORTING: invalid config: '
                  + '"min_size" must be integer')
            sys.exit()
    elif config['min_size'] < 0:
        print('hd_recover: ABORTING: invalid config: '
              + '"min_size" must be 0 or larger')
        sys.exit()
    elif type(config['dir_size']) is not int:
        try:
            config['dir_size'] = int(config['dir_size'])
        except ValueError:
            print('hd_recover: ABORTING: invalid config: '
                  + '"dir_size" must be integer')
            sys.exit()
    elif config['dir_size'] < 0:
        print('hd_recover: ABORTING: invalid config: '
              + '"dir_size" must be 0 or larger')
        sys.exit()
    elif type(config['img_mins']) is not dict:
        print('hd_recover: ABORTING: invalid config: '
              + '"img_mins" must be map of key-value pairs')
        sys.exit()
    elif type(config['img_mins']['pixels']) is not int:
        try:
            config['img_mins']['pixels'] = int(config['img_mins']['pixels'])
        except ValueError:
            print('hd_recover: ABORTING: invalid config: '
                  + '"img_mins: pixels" must be integer')
            sys.exit()
    elif config['img_mins']['pixels'] < 0:
        print('hd_recover: ABORTING: invalid config: '
              + '"img_mins: pixels" must be 0 or larger')
        sys.exit()
    elif type(config['img_mins']['size']) is not int:
        try:
            config['img_mins']['size'] = int(config['img_mins']['size'])
        except ValueError:
            print('hd_recover: ABORTING: invalid config: '
                  + '"img_mins: size" must be integer')
            sys.exit()
    elif config['img_mins']['size'] < 0:
        print('hd_recover: ABORTING: invalid config: '
              + '"img_mins: size" must be 0 or larger')
        sys.exit()
    elif type(config['ignore_blanks']) is not bool:
        print('hd_recover: ABORTING: invalid config: '
              + '"ignore_blanks" must be boolean value')
        sys.exit()
    elif type(config['no_skipped']) is not bool:
        print('hd_recover: ABORTING: invalid config: '
              + '"no_skipped" must be boolean value')
        sys.exit()
    elif type(config['ignore_thumbnails']) is not bool:
        print('hd_recover: ABORTING: invalid config: '
              + '"ignore_thumbnails" must be boolean value')
        sys.exit()
    elif type(config['img_formats']) is not list:
        print('hd_recover: ABORTING: invalid config: '
              + '"img_formats" must be list of strings')
        sys.exit()
    elif type(config['extensions']) is not dict:
        print('hd_recover: ABORTING: invalid config: '
              + '"extensions" must be a map of key-value pairs')
        sys.exit()
    else:
        for format_ in config['img_formats']:
            if type(format_) is not str:
                print('hd_recover: ABORTING: invalid config: '
                      + '"img_formats" must be list of strings')
                sys.exit()
        for extension in config['extensions']:
            if type(config['extensions'][extension]) is not bool:
                print('hd_recover: ABORTING: invalid config: '
                      + 'extension values must be boolean')
                sys.exit()

    return config


def _populate_missing_config(config):
    """
    Load defaults for any missing configuration settings. Useful
    when incomplete but otherwise valid configuration file is loaded.

    :param config dict: potentially incomplete map of configuration settings
    :returns config dict: complete map of configuration settings
    """
    try:
        if 'indent' not in config:
            config['indent'] = DEFAULT_CONFIG['indent']

        if 'min_size' not in config:   # min_size
            config['min_size'] = DEFAULT_CONFIG['min_size']

        if 'dir_size' not in config:   # dir_size
            config['dir_size'] = DEFAULT_CONFIG['dir_size']

        if 'ignore_blanks' not in config:  # ignore_blanks
            config['ignore_blanks'] = DEFAULT_CONFIG['ignore_blanks']

        if 'no_skipped' not in config:  # no_skipped
            config['no_skipped'] = DEFAULT_CONFIG['no_skipped']

        if 'ignore_thumbnails' not in config:  # ignore_thumbnails
            config['ignore_thumbnails'] = \
                DEFAULT_CONFIG['ignore_thumbnails']

        if 'img_mins' not in config:   # img_mins
            config['img_mins'] = {}

        if 'pixels' not in config['img_mins']:     # img_mins[pixels]
            config['img_mins']['pixels'] = \
                DEFAULT_CONFIG['img_mins']['pixels']

        if 'size' not in config['img_mins']:       # img_mins[size]
            config['img_mins']['size'] = \
                DEFAULT_CONFIG['img_mins']['size']

        if 'img_formats' not in config:     # img_formats
            config['img_formats'] = DEFAULT_CONFIG['img_formats']

        if 'extensions' not in config:     # extensions
            config['extensions'] = DEFAULT_CONFIG['extensions']
        for extension in DEFAULT_CONFIG['extensions']:
            if extension not in config['extensions']:
                config['extensions'][extension] = True
    except KeyError:
        print('hd_recover: ABORTING: default configuration file corrupted')
        sys.exit()

    return config

# --- END PRIVATE METHODS --- #


# --- BEGIN PUBLIC METHODS --- #

def load_config(filepath=None, indent=None, ignore_blanks=None,
                ignore_thumbnails=None, file_size=None, pixels=None,
                img_size=None, extensions=None):
    """
    Build configuration map from configuration file and command line options.
    Ensures complete, valid configuration loaded.

    :param ignore_blanks bool: whether to ignore blank id3 tags on mp3s
    :param ignore_thumbnails bool: whether to ignore images of profile
        specified by img_formats, img_size, and pixels
    :param file_size int: minimum file size in bytes to operate on
    :param pixels int: minimum image dimensions in pixels
    :param img_size int: minimum image size in bytes
    :param img_formats list: list of image format extensions to check
                            for thumbnails
    :param extensions list: list of all file extensions to operate on
    :returns config dict: complete, valid configuration map
    """

    if filepath:
        try:
            with open(os.path.join(os.curdir, filepath), 'r') as conf:
                config = yaml.load(conf)
        except FileNotFoundError:
            print('hd_recover: failed to load configuration from ' + filepath)
            sys.exit()
    else:
        filename = inspect.getframeinfo(inspect.currentframe()).filename
        path = os.path.dirname(os.path.abspath(filename))
        try:
            with open(os.path.join(path, 'hd_recover.yaml'), 'r') as conf:
                config = yaml.load(conf)
        except FileNotFoundError:     # Load defaults if no config file
            config = DEFAULT_CONFIG

    # Set defaults on missing fields
    config = _populate_missing_config(config)

    # Override config with command line options if specified
    if indent:
        config['indent'] = indent
    if ignore_blanks:
        config['ignore_blanks'] = ignore_blanks
    if ignore_thumbnails:
        config['ignore_thumbnails'] = ignore_thumbnails
    if file_size:
        config['min_size'] = file_size
    if pixels:
        config['img_mins']['pixels'] = pixels
    if img_size:
        config['img_mins']['size'] = img_size
    if extensions:
        config['extensions'] = {}
        for extension in extensions:
            config['extensions'][extension] = True

    print('\nSIZE: ' + str(config['min_size']) + '\n')
    _validate_config(config)     # Validate

    return config

# ----- END PUBLIC METHOD ----- #


# ----- BEGIN DEFAULT CONFIG ----- #

DEFAULT_CONFIG = {
    'indent': 0,
    'min_size': 5000,   # bytes
    'dir_size': 500,
    'ignore_blanks': False,
    'no_skipped': False,
    'ignore_thumbnails': False,
    'img_mins': {
        'pixels': 100,
        'size': 20000,  # bytes
    },
    'img_formats': [
        'bmp', 'eps', 'gif', 'icns', 'ico', 'jpg', 'pcx', 'png', 'ppm', 'tif'
    ],
    'extensions': {
        'unknown': True,
        '7z': True,     # ARCHIVE
        'a': True,
        'ace': True,
        'arj': True,
        'bkf': True,
        'bz2': True,
        'cab': True,
        'dar': True,
        'deb': True,
        'dump': True,
        'gz': True,
        'lzh': True,
        'lzo': True,
        'par2': True,
        'rar': True,
        'rpm': True,
        'stu': True,
        'tar': True,
        'tar.gz': True,
        'vbm': True,
        'wim': True,
        'xar': True,
        'xz': True,
        'zip': True,
        '3ds': True,    # MULTIMEDIA
        '3dm': True,
        '3g2': True,
        '3gp': True,
        'abr': True,
        'acb': True,
        'ado': True,
        'aep': True,
        'aif': True,
        'albm': True,
        'all': True,
        'als': True,
        'ani': True,
        'ape': True,
        'ari': True,
        'arw': True,
        'asf': True,
        'asl': True,
        'au': True,
        'avi': True,
        'axp': True,
        'binvox': True,
        'bdm': True,
        'bld': True,
        'blend': True,
        'bmp': True,
        'bpg': True,
        'c4d': True,
        'caf': True,
        'cam': True,
        'camrec': True,
        'CATDrawing': True,
        'cda': True,
        'cdd': True,
        'cdl': True,
        'cdr': True,
        'cdt': True,
        'che': True,
        'comicdoc': True,
        'cpi': True,
        'cpr': True,
        'cr2': True,
        'crw': True,
        'csh': True,
        'ctg': True,
        'cue': True,
        'dad': True,
        'db': True,
        'dcm': True,
        'dcr': True,
        'djv': True,
        'dng': True,
        'dp': True,
        'dpx': True,
        'ds2': True,
        'dsc': True,
        'dss': True,
        'ds_store': True,
        'dta': True,
        'dv': True,
        'dvi': True,
        'dvr': True,
        'dwg': True,
        'emf': True,
        'epub': True,
        'ers': True,
        'exs': True,
        'fcp': True,
        'fh10': True,
        'fh5': True,
        'flac': True,
        'fla': True,
        'flp': True,
        'flv': True,
        'gi': True,
        'gif': True,
        'gp4': True,
        'gp5': True,
        'gpx': True,
        'gsm': True,
        'icc': True,
        'icns': True,
        'ico': True,
        'idf': True,
        'idx': True,
        'iff': True,
        'ind': True,
        'ifo': True,
        'indd': True,
        'info': True,
        'ipt': True,
        'iso': True,
        'it': True,
        'itu': True,
        'ora': True,
        'jng': True,
        'jpg': True,
        'jpg': True,
        'kra': True,
        'logic': True,
        'm2t': True,
        'm2ts': True,
        'm3u': True,
        'max': True,
        'max': True,
        'mb': True,
        'mfa': True,
        'mhbd': True,
        'mid': True,
        'mkv': True,
        'mlv': True,
        'mng': True,
        'mov': True,
        'mp': True,
        'mp3': True,
        'mp4': True,
        'mpg': True,
        'mpl': True,
        'mpo': True,
        'mrw': True,
        'mus': True,
        'mws': True,
        'nef': True,
        'oci': True,
        'ogg': True,
        'ogm': True,
        'ogv': True,
        'orf': True,
        'pbm': True,
        'pct': True,
        'pcx': True,
        'psb': True,
        'pef': True,
        'pgm': True,
        'png': True,
        'pnm': True,
        'ppm': True,
        'prproj': True,
        'psd': True,
        'psf': True,
        'psp': True,
        'ptb': True,
        'pts': True,
        'pvp': True,
        'qcp': True,
        'qkt': True,
        'qxd': True,
        'qxp': True,
        'r3d': True,
        'raf': True,
        'ram': True,
        'ra': True,
        'raw': True,
        'rdc': True,
        'rm': True,
        'rns': True,
        'rns': True,
        'rpp': True,
        'rw2': True,
        'rx2': True,
        'ses': True,
        'shn': True,
        'sib': True,
        'sit': True,
        'skd': True,
        'smil': True,
        'spss': True,
        'sr2': True,
        'svg': True,
        'swc': True,
        'swf': True,
        'tg': True,
        'tif': True,
        'TiVo': True,
        'tod': True,
        'tpl': True,
        'ts': True,
        'vdj': True,
        'wav': True,
        'wdp': True,
        'webm': True,
        'wee': True,
        'wmf': True,
        'wnk': True,
        'wpb': True,
        'wpl': True,
        'wtv': True,
        'wv': True,
        'x3f': True,
        'xcf': True,
        'xm': True,
        'xmp': True,
        'xrns': True,
        'xv': True,
        'zcode': True,
        'accdb': True,  # OFFICE
        'ai': True,
        'apr': True,
        'csv': True,
        'cwk': True,
        'doc': True,
        'docx': True,
        'fb2': True,
        'fods': True,
        'fp7': True,
        'fp12': True,
        'kmy': True,
        'lyx': True,
        'mdb': True,
        'njx': True,
        'odg': True,
        'odp': True,
        'ods': True,
        'odt': True,
        'one': True,
        'pages': True,
        'pap': True,
        'ppt': True,
        'pptx': True,
        'pub': True,
        'qbb': True,
        'qbw': True,
        'qpw': True,
        'rtf': True,
        'sda': True,
        'sdc': True,
        'sdd': True,
        'sdw': True,
        'slk': True,
        'sav': True,
        'snt': True,
        'sxc': True,
        'sxd': True,
        'sxi': True,
        'sxw': True,
        'tex': True,
        'txt': True,
        'vsd': True,
        'vsdx': True,
        'wpd': True,
        'wps': True,
        'xlr': True,
        'xls': True,
        'xlsx': True,
        'wdb': True,
        'wk4': True,
        'wks': True,
        '1cd': True,    # OTHER
        'abcdp': True,
        'ab': True,
        'adr': True,
        'agn': True,
        'ahn': True,
        'amb': True,
        'amd': True,
        'amr': True,
        'amt': True,
        'apa': True,
        'apple': True,
        'asm': True,
        'asp': True,
        'atd': True,
        'atd': True,
        'att': True,
        'axx': True,
        'bac': True,
        'bat': True,
        'bim': True,
        'brd': True,
        'c': True,
        'chm': True,
        'class': True,
        'cls': True,
        'cm': True,
        'compress': True,
        'cow': True,
        'cp_': True,
        'd2s': True,
        'dat': True,
        'dbf': True,
        'dbn': True,
        'dbx': True,
        'dc': True,
        'ddf': True,
        'dex': True,
        'dgn': True,
        'dif': True,
        'dim': True,
        'diskimage': True,
        'dll': True,
        'dmp': True,
        'drw': True,
        'dsa': True,
        'dst': True,
        'dxf': True,
        'e01': True,
        'ecr': True,
        'eCryptfs': True,
        'edb': True,
        'elf': True,
        'emb': True,
        'emka': True,
        'emlx': True,
        'eps': True,
        'evt': True,
        'exe': True,
        'fbf': True,
        'fbk': True,
        'fcs': True,
        'fdb': True,
        'fds': True,
        'f': True,
        'fh1': True,
        'fits': True,
        'fob': True,
        'fos': True,
        'fp5': True,
        'freeway': True,
        'frm': True,
        'frm': True,
        'fst': True,
        'fs': True,
        'fwd': True,
        'gam': True,
        'gcs': True,
        'gct': True,
        'gho': True,
        'gm6': True,
        'gm81': True,
        'gmd': True,
        'gmk': True,
        'gp2': True,
        'gpg': True,
        'gsb': True,
        'h': True,
        'hdf': True,
        'hdr': True,
        'hds': True,
        'hm': True,
        'hr9': True,
        'html.gz': True,
        'html': True,
        'http': True,
        'ibd': True,
        'ics': True,
        'imb': True,
        'img': True,
        'imm': True,
        'inf': True,
        'ini': True,
        'jad': True,
        'jar': True,
        'jks': True,
        'jnb': True,
        'jp2': True,
        'json': True,
        'jsonlz4': True,
        'jsp': True,
        'kdb': True,
        'kdbx': True,
        'key': True,
        'kmz': True,
        'ldf': True,
        'lit': True,
        'lnk': True,
        'lso': True,
        'luks': True,
        'lwo': True,
        'lxo': True,
        'ly': True,
        'mat': True,
        'mcd': True,
        'mdf': True,
        'mdl': True,
        'mem': True,
        'mfg': True,
        'mig': True,
        'mk5': True,
        'mmap': True,
        'mny': True,
        'mobi': True,
        'msf': True,
        'msg': True,
        'mxf': True,
        'MYI': True,
        'myo': True,
        'nd2': True,
        'nds': True,
        'nes': True,
        'nk2': True,
        'notebook': True,
        'nsf': True,
        'p65': True,
        'paf': True,
        'pcap': True,
        'pcb': True,
        'pcp': True,
        'pdf': True,
        'pds': True,
        'pf': True,
        'pfx': True,
        'pgp': True,
        'php': True,
        'pli': True,
        'plist': True,
        'pl': True,
        'plt': True,
        'pm': True,
        'ppk': True,
        'prc': True,
        'prd': True,
        'prt': True,
        'psmodel': True,
        'ps': True,
        'pst': True,
        'ptf': True,
        'ptx': True,
        'pub': True,
        'pyc': True,
        'py': True,
        'pzf': True,
        'pzh': True,
        'qbb': True,
        'qbmb': True,
        'qbw': True,
        'qdf-backup': True,
        'qdf': True,
        'qgs': True,
        'rb': True,
        'RData': True,
        'reg': True,
        'res': True,
        'rfp': True,
        'rlv': True,
        'rsa': True,
        'rvt': True,
        'save': True,
        'schematic': True,
        'sgcta': True,
        'sh3d': True,
        'sh': True,
        'skp': True,
        'sla': True,
        'sldprt': True,
        'sld': True,
        'sp3': True,
        'sparseimage': True,
        'spe': True,
        'spf': True,
        'sqlite': True,
        'sql': True,
        'sqm': True,
        'steuer2014': True,
        'steuer2015': True,
        'stl': True,
        'stp': True,
        'studio': True,
        'tax': True,
        'tcw': True,
        'tib': True,
        'ticket.bin': True,
        'torrent': True,
        'tph': True,
        'ttd': True,
        'ttf': True,
        'tz': True,
        'url': True,
        'v2i': True,
        'vault': True,
        'vb': True,
        'vcf': True,
        'vdi': True,
        'veg': True,
        'vfb': True,
        'vib': True,
        'wallet': True,
        'vmdk': True,
        'vmg': True,
        'wab': True,
        'wim': True,
        'win': True,
        'wld': True,
        'wma': True,
        'wmv': True,
        'woff': True,
        'x4a': True,
        'x4g': True,
        'x4p': True,
        'x4s': True,
        'xfi': True,
        'xml.gz': True,
        'xml': True,
        'xoj': True,
        'xpi': True,
        'xpt': True,
        'xsv': True,
        'z2d': True,
        'zpr': True
        }
}

# ----- END DEFAULT CONFIG ----- #
