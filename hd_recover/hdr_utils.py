import os
import sys
import json
from PIL import Image
from hsaudiotag import mpeg as mp3Tag
from datetime import datetime, timezone
from configuration import DEFAULT_CONFIG


# ----- BEGIN UTILITY FUNCTIONS ----- #

def sizeof(filepath):
    """
    Wrapper for os.stat. Get size of file with filename.
    Assumes full path relative to current working directory (cwd).

    :param filepath str: path to file relative to current working directory
    :returns int: file size in bytes
    """
    try:
        return os.stat(filepath).st_size
    except FileNotFoundError:
        return 0


def dateof(filepath):
    """
    Get modify date of file at filepath and convert to truncated ISO 8601.

    :param filepath str: path to file relative to current working directory
    :returns str: date formatted as YYYY-MM-DD
    """
    try:
        timestamp = os.stat(filepath).st_mtime
    except FileNotFoundError:
        timestamp = 0

    today = datetime.today().replace(tzinfo=timezone.utc).timestamp()
    if timestamp >= 0 and timestamp <= today:
        date = datetime.fromtimestamp(timestamp)
    else:       # default if corrupted timestamp
        date = datetime.fromtimestamp(0)
        # date = datetime.fromisoformat('1970-01-01')
    return str(date).split(' ')[0]  # return date, strip time details


def strip_extension(filename):
    """
    Retrieve extension from filename or return 'unknown' if none.

    :param filename str: name of file to check for extension
    :returns str: extension signature, or unknown if none
    """
    split_name = filename.split('.')
    if len(split_name) > 1:
        return split_name.pop().lower()  # strip extension
    else:
        return 'unknown'


def format_size(size):
    """
    Convert size from bytes to largest unit >= 1, stringify,
    and append unit abbreviation. E.g. 2000 converted to
    '2K'.

    :param size int: size of a file in bytes
    :returns str: size converted to largest unit where value >= 1
    """

    size = float(size)
    if size < 1000:
        return '{:.2f}'.format(size) + 'B'
    elif size < 1000000:
        return '{:.2f}'.format(size / 1000) + 'K'
    elif size < 1000000000:
        return '{:.2f}'.format(size / 1000000) + 'M'
    elif size < 1000000000000:
        return '{:.2f}'.format(size / 1000000000) + 'G'
    else:
        return '{:.2f}'.format(size / 1000000000000) + 'T'


def has_id3(filepath):
    """
    Check whether file at filepath has a complete id3 tag, i.e. artist and
    title.

    :param filepath str: path to file
    :returns bool: whether file does or does not have a complete id3 tag
    """
    try:
        mp3 = mp3Tag.Mpeg(filepath)
    except FileNotFoundError:
        raise

    if mp3.tag is None:
        return False
    else:
        return True


def is_thumbnail(filepath, config):
    """
    Check whether file at filepath should matches the thumbnail criteria
    specified in the configuration settings, i.e. 'img_mins'.

    :param filepath str: path to file
    :param config dict: map of configuration settings
    :returns bool: whether file is or is not a thumbnail
    """
    size = sizeof(filepath)
    try:
        image = Image.open(filepath)
        min_pixels = config['img_mins']['pixels']
        if size < config['img_mins']['size']:
            return True
        elif image.size[0] < min_pixels and image.size[1] < min_pixels:
            return True
        else:
            return False
    except OSError:
        raise


def is_ignored(filepath, config):
    """
    Check whether file at filepath should be excluded from analysis or sort
    based on configuration settings.

    :param filepath str: path to file
    :param extension str: extension signature of file
    :param config dict: map of configuration settings
    :param extensions list: list of extensions to include in analysis or sort
    :returns bool: whether file should or should not be ignored
    """

    size = sizeof(filepath)
    if size < config['min_size'] or size <= 0:
        return True

    extension = strip_extension(filepath)
    if extension == 'mp3':
        if config['ignore_blanks'] is True \
           and not has_id3(filepath):
            return True

    if config['ignore_thumbnails'] is True:
        if extension in config['img_formats'] \
           and is_thumbnail(filepath) is True:
            return True

    if extension not in config['extensions']:
        return True
    elif config['extensions'][extension] is not True:
        return True

    return False

# ----- END UTILITY FUNCTIONS ----- #


# ----- BEGIN IMPORT FUNCTIONS ----- #

def _is_valid_import(analysis):
    """
    Verify whether imported JSON analysis data matches expected data model.

    :param analysis dict: analysis data map loaded from JSON file
    :returns bool: whether the analysis fits expected data model
    """
    if not analysis:
        return False
    elif type(analysis) is not dict:
        return False

    try:
        fields = ('count', 'size', 'percent')
        analysis_keys = ('extensions', 'dates', 'ignored', 'total_size',
                         'config')

        if not all(key in analysis for key in analysis_keys):
            return False

        if type(analysis['extensions']) is not dict:
            return False
        elif len(analysis['extensions']) <= 0:
            return False

        if type(analysis['dates']) is not dict:
            return False
        elif len(analysis['dates']) <= 0:
            return False

        if type(analysis['ignored']) is not dict:
            return False

        if type(analysis['total_size']) is not int:
            return False

        for extension in analysis['extensions']:
            if type(extension) is not str:
                return False
            if not all(field in analysis['extensions'][extension]
                       for field in fields):
                return False

            if type(analysis['extensions'][extension]['count']) is not int:
                return False
            elif type(analysis['extensions'][extension]['size']) is not int:
                return False
            elif type(analysis['extensions'][extension]['percent']) \
                    is not float:
                return False

        for date in analysis['dates']:
            if type(date) is not str:
                return False
            if not all(field in analysis['dates'][date]
                       for field in fields):
                return False

            if type(analysis['dates'][date]['count']) is not int:
                return False
            elif type(analysis['dates'][date]['size']) is not int:
                return False
            elif type(analysis['dates'][date]['percent']) \
                    is not float:
                return False

        if not all(field in analysis['ignored'] for field in fields):
            return False
        if type(analysis['ignored']['count']) is not int:
            return False
        elif type(analysis['ignored']['size']) is not int:
            return False
        elif type(analysis['ignored']['percent']) \
                is not float:
            return False

        if type(analysis['config']) is not dict:
            return False
        if not all(key in analysis['config'] for key in DEFAULT_CONFIG):
            return False

    except KeyError:
        return False

    return True


def import_from_json(filepath):
    """
    Import metadata from JSON file.

    :param filepath str: name of JSON file to import assessment details from
    :returns analysis dict: map of analysis results from analyze command
    """

    print(
        'hd_recover assess: importing data from {0}... '.format(filepath),
        end='')
    try:
        with open(filepath, 'r') as json_file:
            try:
                analysis = json.load(json_file)
            except (json.decoder.JSONDecodeError, UnicodeDecodeError):
                print('FAILED: input must be valid JSON')
                sys.exit()
    except FileNotFoundError:
        print(
            'FAILED: --input {}: file not found'.format(filepath)
        )
        sys.exit()

    if _is_valid_import(analysis):
        print('OK')
        return analysis
    else:
        print('FAILED: corrupted or incompatible JSON file')
        sys.exit()

# ----- END IMPORT FUNCTIONS ----- #
